//! Handles argument conversion to `OsString` and `String`.

#[cfg(unix)]
unsafe fn ptr_to_os_string(ptr: *const u8) -> std::ffi::OsString {
    use std::os::unix::ffi::OsStrExt;
    let len = {
        let mut len = 0;
        let mut ptr = ptr;

        while ptr.read() != 0 {
            len += 1;
            ptr = ptr.add(1);
        }

        len
    };

    let slice = std::slice::from_raw_parts(ptr, len);
    std::ffi::OsStr::from_bytes(slice).to_os_string()
}

#[cfg(windows)]
unsafe fn ptr_to_os_string(ptr: *const u8) -> std::ffi::OsString {
    use std::os::windows::ffi::OsStringExt;
    let ptr: *const u16 = ptr as *const u16;

    let len = {
        let mut len = 0;
        let mut ptr = ptr;

        while ptr.read() != 0 {
            len += 1;
            ptr = ptr.add(1);
        }

        len
    };
    
    let slice = std::slice::from_raw_parts(ptr, len);
    std::ffi::OsString::from_wide(slice)
}

/// Parses `#![no_main]` C-style arguments into a `Vec` of `OsString`s.
pub fn parse_args_os(argc: isize, argv: *const *const u8) -> Vec<std::ffi::OsString> {
    use std::ffi::OsString;

    if argc < 0 {
        // this should never actually run, but just in case
        panic!("how");
    }

    // Convert argc into a usize since it won't be negative.
    let argc = argc as usize;

    // Allocate the argument array
    let mut args = Vec::with_capacity(argc);

    // Convert the items in `argv` to `OsString`s
    // Requires unsafe since we're using pointers and stuff
    unsafe {
        for i in 0..argc {
            // Get the `*const u8` to the argument
            let ptr = argv.add(i).read(); 
            args.push(ptr_to_os_string(ptr));
        }
    }

    args
}

/// Parses `#![no_main]` C-style arguments into a `Vec` of `String`s.
pub fn parse_args(argc: isize, argv: *const *const u8) -> Vec<String> {
    parse_args_os(argc, argv).into_iter()
        .map(|s| s.to_str().expect("invalid unicode in argument").to_owned())
        .collect()
}
