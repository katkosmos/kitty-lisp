use std::io::Read;
use ufmt::{uDebug, uDisplay, uWrite};
use utf8_read::{Char, Reader};

pub enum Error {
    Utf8Read(utf8_read::Error),
    InvalidNumber,
}

impl uDisplay for Error {
    fn fmt<W: ?Sized>(&self, f: &mut ufmt::Formatter<W>) -> Result<(), W::Error>
    where
        W: uWrite,
    {
        match self {
            Error::Utf8Read(e) => uwrite!(f, "{}", e),
            Error::InvalidNumber => uwrite!(f, "Invalid number"),
        }
    }
}

/// A token
#[derive(Clone, PartialEq)]
pub enum Token {
    /// A string
    String(String),
    /// A number of any type, given as a string representation.
    Number(f64, String),
    /// Boolean
    Boolean(bool),
    /// Open parenthesis
    OpenParenthesis,
    /// Close parenthesis
    CloseParenthesis,
    /// Comma
    Comma,
    /// Period/Dot
    Dot,
    /// Symbol
    Symbol(String),
    /// End-of-file
    EOF,
    /// Whitespace
    Whitespace,
    /// End-oif-list object
    Nil,
}

impl uDebug for Token {
    fn fmt<W: ?Sized>(&self, f: &mut ufmt::Formatter<W>) -> Result<(), <W as uWrite>::Error>
    where
        W: uWrite,
    {
        match self {
            Token::String(string) => {
                f.write_str("String(\"")?;
                f.write_str(string)?;
                f.write_str("\")")
            }

            Token::Number(_, num) => {
                f.write_str("Number(")?;
                f.write_str(num)?;
                f.write_str(", \"")?;
                f.write_str(num)?;
                f.write_str("\")")
            }

            Token::Symbol(symbol) => {
                f.write_str("Symbol(\"")?;
                f.write_str(symbol)?;
                f.write_str("\")")
            }

            Token::Boolean(boolean) => uwrite!(f, "Boolean({:?})", boolean),
            Token::Whitespace => uwrite!(f, "Whitespace"),
            Token::EOF => uwrite!(f, "EOF"),
            Token::Dot => uwrite!(f, "Dot"),
            Token::Comma => uwrite!(f, "Comma"),
            Token::OpenParenthesis => uwrite!(f, "OpenParenthesis"),
            Token::CloseParenthesis => uwrite!(f, "CloseParenthesis"),
            Token::Nil => uwrite!(f, "Nil"),
        }
    }
}

/// A streaming lexer
pub struct Lexer<R: Read> {
    /// Allows for reading the input character-by-character
    reader: Reader<R>,
    /// The last characters read
    last_chars: Vec<char>,
    /// Line number
    line: usize,
    /// Column number
    column: usize,
}

impl<R: Read> Lexer<R> {
    /// Creates and returns a new lexer object
    pub fn new(src: R) -> Self {
        Self {
            reader: Reader::new(src),
            last_chars: Vec::new(),
            line: 0,
            column: 0,
        }
    }

    /// Peeks the next token
    pub fn peek(&mut self) -> Result<Option<Token>, Error> {
        let token = self.next()?;

        match token {
            None | Some(Token::EOF) => (),
            Some(Token::Whitespace) => self.last_chars.insert(0, ' '),
            Some(Token::OpenParenthesis) => self.last_chars.insert(0, '('),
            Some(Token::CloseParenthesis) => self.last_chars.insert(0, ')'),
            Some(Token::Comma) => self.last_chars.insert(0, ','),
            Some(Token::Dot) => self.last_chars.insert(0, '.'),
            Some(Token::String(ref string)) => {
                self.last_chars.insert(0, '"');

                for ch in string.chars().rev() {
                    self.last_chars.insert(0, ch);

                    if ch == '"' {
                        self.last_chars.insert(0, '\\');
                    }
                }

                self.last_chars.insert(0, '"');
            }

            Some(Token::Number(_, ref lexeme)) => {
                for ch in lexeme.chars().rev() {
                    self.last_chars.insert(0, ch);
                }
            }

            Some(Token::Boolean(boolean)) => {
                if boolean {
                    self.last_chars.insert(0, 'e');
                    self.last_chars.insert(0, 'u');
                    self.last_chars.insert(0, 'r');
                    self.last_chars.insert(0, 't');
                } else {
                    self.last_chars.insert(0, 'e');
                    self.last_chars.insert(0, 's');
                    self.last_chars.insert(0, 'l');
                    self.last_chars.insert(0, 'a');
                    self.last_chars.insert(0, 'f');
                }

                self.last_chars.insert(0, '#');
            }

            Some(Token::Nil) => {
                self.last_chars.insert(0, 'L');
                self.last_chars.insert(0, 'I');
                self.last_chars.insert(0, 'N');
            }

            Some(Token::Symbol(ref symbol)) => {
                for ch in symbol.chars().rev() {
                    self.last_chars.insert(0, ch);
                }
            }
        }

        Ok(token)
    }

    /// Returns the next token
    #[allow(clippy::should_implement_trait)]
    pub fn next(&mut self) -> Result<Option<Token>, Error> {
        // Skip whitespace and return a whitespace token
        if self.skip_whitespace()? {
            return Ok(Some(Token::Whitespace));
        }

        let peek = self.peek_char()?;
        if let Some(peek) = peek {
            match peek {
                // Strings
                '"' => Ok(self.string()?.map(Token::String)),

                // Numbers
                '0' | '1' | '2' | '3' | '4' | '5' | '6' | '7' | '8' | '9' => match self.number()? {
                    None => Ok(None),
                    Some(n) => {
                        let parsed_n = match n.parse() {
                            Ok(n) => Ok(n),
                            Err(_) => Err(Error::InvalidNumber),
                        }?;

                        Ok(Some(Token::Number(parsed_n, n)))
                    }
                },

                '(' => {
                    self.advance_char()?;
                    Ok(Some(Token::OpenParenthesis))
                }

                ')' => {
                    self.advance_char()?;
                    Ok(Some(Token::CloseParenthesis))
                }

                '.' => {
                    self.advance_char()?;
                    Ok(Some(Token::Dot))
                }

                ',' => {
                    self.advance_char()?;
                    Ok(Some(Token::Comma))
                }

                '#' => {
                    self.advance_char()?;
                    Ok(self.symbol()?.and_then(|symbol| match &symbol[..] {
                        "true" => Some(Token::Boolean(true)),
                        "false" => Some(Token::Boolean(false)),
                        _ => None,
                    }))
                }

                _ => {
                    let symbol = self.symbol()?;
                    Ok(if let Some(symbol) = symbol {
                        match &symbol[..] {
                            "NIL" => Some(Token::Nil),
                            _ => Some(Token::Symbol(symbol)),
                        }
                    } else {
                        self.advance_char()?;
                        None
                    })
                }
            }
        } else {
            Ok(Some(Token::EOF))
        }
    }

    /// Returns the current line and column numbers
    pub fn line_col(&self) -> (usize, usize) {
        (self.line, self.column)
    }

    /// Lexes a symbol
    fn symbol(&mut self) -> Result<Option<String>, Error> {
        let symbol = self.match_while(|ch| {
            !ch.is_whitespace()
                && ch != '('
                && ch != ')'
                && ch != '.'
                && ch != ','
                && ch != '#'
                && ch != '"'
        })?;

        if symbol.is_empty() {
            Ok(None)
        } else {
            Ok(Some(symbol))
        }
    }

    /// Lexes a string
    fn string(&mut self) -> Result<Option<String>, Error> {
        let idx = self.last_chars.len();

        if Some('"') == self.peek_char()? {
            // Advance past the first quote
            self.advance_char()?;
            // Match while not a closing quote
            let mut string = self.match_while(|ch| ch != '"')?;
            while string.ends_with('\\') && self.peek_char()? == Some('"') {
                // Handle escapes properly
                string = string.replace(r"\\", r"\");
                if !string.ends_with('\\') {
                    break;
                }

                // If the string ends with a backslash (escaped quote),
                // pop the backslash off and push a quote on,
                // then continue parsing the string
                string.pop();
                string.push('"');
                self.advance_char()?;

                string.push_str(&self.match_while(|ch| ch != '"')?);
            }

            // Check if we have a closing quote
            if self.peek_char()? == Some('"') {
                self.advance_char()?;
                return Ok(Some(string));
            } else {
                // Restore the consumed characters back to the peek queue
                self.last_chars.insert(idx, '"');
                let mut idx = idx + 1;

                for ch in string.chars() {
                    if ch == '"' {
                        self.last_chars.insert(idx, '\\');
                        idx += 1;
                    }

                    self.last_chars.insert(idx, ch);
                    idx += 1;
                }
            }
        }

        Ok(None)
    }

    /// Lexes a number
    fn number(&mut self) -> Result<Option<String>, Error> {
        // Check if there's a leading negative
        let (neg, neg_idx) = if Some('-') == self.peek_char()? {
            self.advance_char()?;
            (true, self.last_chars.len())
        } else {
            (false, 0)
        };

        // Decimal
        let mut string = self.match_while(|ch| ch.is_ascii_digit())?;

        if !string.is_empty() {
            // Handle fractional components
            let idx = self.last_chars.len();
            if Some('.') == self.peek_char()? {
                self.advance_char()?;
                let decimal_string = self.match_while(|ch| ch.is_ascii_digit())?;
                if !decimal_string.is_empty() {
                    string.push('.');
                    string.push_str(&decimal_string);
                } else {
                    // Restore consumed character
                    self.last_chars.insert(idx, '.');
                }
            }

            // Handle exponents
            let idx = self.last_chars.len();
            if Some('e') == self.peek_char()? || Some('E') == self.peek_char()? {
                self.advance_char()?;

                // Consume a sign if there is one
                let sign = if Some('+') == self.peek_char()? || Some('-') == self.peek_char()? {
                    self.advance_char()?
                } else {
                    None
                };

                // Consume the exponent number
                let exp_string = self.match_while(|ch| ch.is_ascii_digit())?;
                if !exp_string.is_empty() {
                    string.push('e');

                    if let Some(sign) = sign {
                        string.push(sign);
                    }

                    string.push_str(&exp_string);
                } else {
                    // Restore consumed characters
                    self.last_chars.insert(idx, 'e');
                    if let Some(sign) = sign {
                        self.last_chars.insert(idx + 1, sign);
                    }
                }
            }

            // Return the final constructed decimal string
            return Ok(Some(string));
        }

        // Restore the negative sign if the string didn't match
        if neg {
            self.last_chars.insert(neg_idx, '-');
        }

        Ok(None)
    }

    /// Skips whitespace
    fn skip_whitespace(&mut self) -> Result<bool, Error> {
        Ok(!self.match_while(|ch| ch.is_whitespace())?.is_empty())
    }

    /// Matches while the provided condition function returns `true`.
    fn match_while<F: Fn(char) -> bool>(&mut self, f: F) -> Result<String, Error> {
        let mut string = String::new();

        while let Some(ch) = self.peek_char()? {
            if f(ch) {
                self.advance_char()?;
                string.push(ch);
            } else {
                break;
            }
        }

        Ok(string)
    }

    /// Matches a string, returning a bool saying whether it could be matched or not
    fn match_exact<S: AsRef<str>>(&mut self, string: S) -> Result<bool, Error> {
        let mut chars = string.as_ref().chars();

        let peeked = self.peek_char()?;
        if peeked == chars.next() {
            if peeked.is_none() {
                return Ok(true);
            }

            let mut peeked_chars = Vec::with_capacity(string.as_ref().chars().count());
            peeked_chars.push(match self.advance_char()? {
                Some(x) => x,
                None => unreachable!(),
            });

            let mut matched = true;

            for ch in chars {
                match self.advance_char() {
                    Ok(adv_ch) => {
                        if let Some(peeked) = adv_ch {
                            peeked_chars.push(peeked);
                            if ch != peeked {
                                matched = false;
                                break;
                            }
                        } else {
                            matched = false;
                            break;
                        }
                    }

                    Err(e) => {
                        self.last_chars.append(&mut peeked_chars);
                        return Err(e);
                    }
                }
            }

            Ok(if !matched {
                self.last_chars.append(&mut peeked_chars);
                false
            } else {
                true
            })
        } else {
            Ok(false)
        }
    }

    /// Advances a character, returning it
    #[inline]
    fn advance_char(&mut self) -> Result<Option<char>, Error> {
        let ch = self.peek_char()?;

        // Increment the column
        self.column += 1;

        // Handle newlines
        if ch == Some('\n') {
            self.column = 0;
            self.line += 1;
        }

        if !self.last_chars.is_empty() {
            self.last_chars.remove(0);
        }

        Ok(ch)
    }

    /// Peeks a character from the source
    #[inline]
    fn peek_char(&mut self) -> Result<Option<char>, Error> {
        if !self.last_chars.is_empty() {
            return Ok(Some(self.last_chars[0]));
        }

        match self.reader.next_char() {
            Ok(Char::Eof) | Ok(Char::NoData) => Ok(None),

            Ok(Char::Char(ch)) => {
                self.last_chars.push(ch);
                Ok(Some(ch))
            }

            Err(e) => Err(Error::Utf8Read(e)),
        }
    }
}
