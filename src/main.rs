#![no_main]

#[macro_use]
extern crate ufmt_stdio;

mod args;
use args::parse_args;

pub mod lexer;
pub use lexer::{Lexer, Token};

pub mod interpreter;
pub use interpreter::Interpreter;

fn print_help(args: &[String]) -> ! {
    println!("Usage: {} [OPTIONS] [FILE]", &*args[0]);
    println!("Options:");
    println!("  -h / --help : Prints this help");
    println!("  --tokens    : Print tokens");
    println!();
    println!("If a file is not provided, an interactive REPL will be started.");
    std::process::exit(0);
}

#[no_mangle]
pub fn main(argc: isize, argv: *const *const u8) {
    ufmt_stdio::init();
    
    // Parse command line arguments
    let (file, should_print_tokens): (Option<std::path::PathBuf>, bool) = {
        let args = parse_args(argc, argv);
        if args.len() == 1 {
            (None, false)
        } else {
            if args.iter()
                .position(|r| r == &String::from("-h") || r == &String::from("--help"))
                .is_some()
            {
                print_help(&args[..]);
            }

            if let Some(i) = args.iter().position(|r| r == &String::from("--tokens")) {
                if i == 1 {
                    if args.len() == 2 {
                        (None, true)
                    } else if args.len() == 3 {
                        (Some(std::path::PathBuf::from(args[2].clone())), true)
                    } else {
                        print_help(&args[..]);
                    }
                } else {
                    print_help(&args[..]);
                }
            } else if args.len() == 2 {
                (Some(std::path::PathBuf::from(args[1].clone())), false)
            } else {
                print_help(&args[..]);
            }
        }
    };

    // Handle the REPL/file execution
    if let Some(file) = file {
        if should_print_tokens {
            let file = std::fs::File::open(&file).unwrap();
            print_tokens(file);
            ufmt_stdio::println!();
            ufmt_stdio::println!("--- Output ---");
        }
        
        let file = std::fs::File::open(&file).unwrap();
        let mut lexer = Lexer::new(file);
        let mut interpreter = Interpreter::new(&mut lexer);
        interpreter.execute();
    } else {
        let stdio = std::io::stdin();
        
        ufmt_stdio::println!("KittyLisp v0.0.1");
        ufmt_stdio::println!("Copyright 2022 katkosmos");

        loop {
            let mut string = String::with_capacity(256);
            ufmt_stdio::print!("> ");
            stdio.read_line(&mut string).unwrap();

            if should_print_tokens {
                print_tokens(string.as_bytes());
                ufmt_stdio::println!();
                ufmt_stdio::println!("--- Output ---");
            }

            let mut lexer = Lexer::new(string.as_bytes());
            let mut interpreter = Interpreter::new(&mut lexer);
            interpreter.execute();
        }
    }
}

fn print_tokens<R: std::io::Read>(x: R) {
    ufmt_stdio::println!("--- Tokens ---");
    let mut lexer = Lexer::new(x);
    loop {
        match lexer.next() {
            Ok(Some(Token::EOF)) => return,
            Ok(Some(t)) => {
                ufmt_stdio::println!("{:?}", t);
            }
            Ok(_) => (),
            Err(_e) => {
                ufmt_stdio::eprintln!("error getting token"); //: {}", e);
                return;
            }
        };
    }
}
