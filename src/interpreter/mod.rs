//! Incorporates an interpreter around a streaming lexer.
//! The interpreter also handles parsing.

use crate::lexer::{Lexer, Token};
use std::io::Read;

mod value;
pub use value::Value;

pub struct Interpreter<'l, R: Read> {
    /// Lexer
    lexer: &'l mut Lexer<R>,
    /// Value stack
    stack: Vec<Value>,
    /// The nesting level of s-expressions
    level: usize,
}

impl<'l, R: Read> Interpreter<'l, R> {
    /// Create a new lexer
    pub fn new(lexer: &'l mut Lexer<R>) -> Self {
        Self {
            lexer,
            stack: Vec::with_capacity(32),
            level: 0,
        }
    }

    /// Execute all expressions
    pub fn execute(&mut self) {
        while match self.lexer.peek() {
            Ok(Some(Token::EOF)) => false,
            Ok(Some(_)) => true,
            Ok(None) => true,
            Err(_) => false,
        } {
            self.expr();
        }
    }

    /// Parse and execute a single expression
    #[inline]
    pub fn expr(&mut self) {
        let token = self.next_token();

        match token {
            Token::Whitespace => (),
            Token::OpenParenthesis => self.s_expr(),

            Token::Nil => {
                self.stack.push(Value::Empty(1));
            }

            Token::String(string) => {
                self.stack.push(Value::String(string));
            }

            Token::Number(num, _) => {
                self.stack.push(Value::Number(num));
            }

            Token::EOF => {
                eprintln!("unexpected EOF");
                std::process::exit(1);
            }

            _ => {
                eprintln!("unimplemented {:?}; TODO", token);
                std::process::exit(1);
            }
        }
    }

    /// Parses an s-expression
    #[inline(always)]
    pub fn s_expr(&mut self) {
        self.level += 1;
        let i = self.stack.len();

        let operation = match self.peek_token() {
            Token::CloseParenthesis => {
                self.next_token();
                self.level -= 1;
                self.stack.push(Value::Empty(1));
                return;
            }

            Token::Symbol(operation) => {
                self.next_token();
                Some(operation)
            }

            _ => {
                self.expr();
                None
            }
        };

        self.whitespace();

        match self.peek_token() {
            Token::Dot => {
                self.next_token();

                if !self.whitespace() {
                    eprintln!("Expected whitespace between '.' and next expression");
                    std::process::exit(1);
                }

                self.expr();
                self.whitespace();
                if self.next_token() != Token::CloseParenthesis {
                    eprintln!("expected ')' to close s-expression");
                    std::process::exit(1);
                }

                let stack_len = self.stack.len();
                if let Some(Value::Empty(ref mut len)) = self.stack.last_mut() {
                    *len = stack_len - i;
                }

                if let Some(operation) = operation {
                    self.run_op(operation);
                }
            }

            _ => {
                while self.peek_token() != Token::CloseParenthesis {
                    self.expr();
                    if self.peek_token() != Token::CloseParenthesis {
                        assert!(self.whitespace(), "Expected whitespace between expressions");
                    }
                }

                self.next_token();

                self.stack.push(Value::Empty(self.stack.len() - i + 1));
                self.level -= 1;

                if let Some(operation) = operation {
                    self.run_op(operation);
                }
            }
        }
    }

    /// Executes the given operation using the values on the stack
    fn run_op<S: AsRef<str>>(&mut self, operation: S) {
        // Get number of arguments
        let arg_n = if let Some(Value::Empty(n)) = self.stack.last() {
            let n = n - 1;
            self.stack.pop();
            n
        } else {
            0
        };

        // Cache the stack length
        let stack_len = self.stack.len();

        {
            let args = &self.stack[stack_len - arg_n..stack_len];

            match operation.as_ref() {
                "+" => {
                    // Add values
                    let sum = args.iter().sum();
                    self.stack.push(sum);
                }

                "-" => {
                    let mut iter = args.iter();
                    let first = iter.next().expect("no items").clone();
                    let sum = iter.sum();
                    self.stack.push(first - sum);
                }

                "write" | "write-line" => {
                    // Print values to stdout
                    for arg in args.iter().take(arg_n - 1) {
                        print!("{}", arg);
                    }

                    if arg_n > 0 {
                        let v = match args.last() {
                            Some(v) => v,
                            None => unreachable!(),
                        };
                        if operation.as_ref() == "write-line" {
                            println!("{}", v);
                        } else {
                            print!("{}", v);
                        }
                    }
                }

                _ => {
                    eprintln!("unsupported operation {}", operation.as_ref());
                    std::process::exit(1);
                }
            }
        }

        for _ in 0..arg_n {
            self.stack.remove(stack_len - arg_n);
        }
    }

    /// Returns the next token
    fn next_token(&mut self) -> Token {
        let mut token = self.lexer.next().unwrap_or(None);
        while token.is_none() {
            token = self.lexer.next().unwrap_or(None);
        }

        token.unwrap()
    }

    /// Peeks the next token
    fn peek_token(&mut self) -> Token {
        let mut token = self.lexer.peek().unwrap_or(None);
        while token.is_none() {
            self.lexer.next().unwrap_or(None);
            token = self.lexer.peek().unwrap_or(None);
        }

        token.unwrap()
    }

    /// Returns true if the next token is whitespace, and skips it.
    fn whitespace(&mut self) -> bool {
        if self.peek_token() == Token::Whitespace {
            self.next_token();
            true
        } else {
            false
        }
    }
}
