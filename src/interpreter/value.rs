//! Value for the interpreter

use ufmt::{uDisplay, uWrite};
use ufmt_float::uFmt_f64;

#[derive(Clone, PartialEq)]
pub enum Value {
    /// A string
    String(String),
    /// A number
    Number(f64),
    /// An empty list
    Empty(usize),
}

impl uDisplay for Value {
    fn fmt<W: ?Sized>(&self, f: &mut ufmt::Formatter<W>) -> Result<(), W::Error>
    where
        W: uWrite,
    {
        match self {
            Value::String(s) => f.write_str(s),
            Value::Number(n) => {
                uwrite!(f, "{}", uFmt_f64::Three(*n))
            }
            Value::Empty(_) => Ok(()),
        }
    }
}

impl std::ops::AddAssign<&Value> for Value {
    fn add_assign(&mut self, rhs: &Value) {
        match self {
            Value::Number(ref mut n) => match rhs {
                Value::Number(other_n) => {
                    *n += other_n;
                }

                Value::String(_s) => todo!(),
                _ => {
                    eprintln!("Unsupported operation");
                    std::process::exit(1);
                }
            },

            Value::String(_s) => match rhs {
                Value::Number(_n) => todo!(),
                Value::String(_other_s) => todo!(),
                _ => {
                    eprintln!("Unsupported operation");
                    std::process::exit(1);
                }
            },

            _ => todo!(),
        }
    }
}

impl std::ops::Sub<Value> for Value {
    type Output = Value;

    fn sub(self, rhs: Value) -> Self::Output {
        match self {
            Value::Number(ref n) => match rhs {
                Value::Number(ref other_n) => Value::Number(n - other_n),
                _ => {
                    eprintln!("Unsupported operation");
                    std::process::exit(1);
                }
            }

            _ => {
                eprintln!("Unsupported operation");
                std::process::exit(1);
            }
        }
    }
}

impl std::iter::Sum<Value> for Value {
    fn sum<I>(mut iter: I) -> Self
    where
        I: Iterator<Item = Value>,
    {
        let mut value = match iter.next() {
            Some(v) => v,
            None => {
                eprintln!("no values to sum");
                std::process::exit(1);
            }
        };

        for v in iter {
            value += &v;
        }

        value
    }
}

impl<'a> std::iter::Sum<&'a Value> for Value {
    fn sum<I>(mut iter: I) -> Self
    where
        I: Iterator<Item = &'a Value>,
    {
        let mut value = match iter.next() {
            Some(v) => v.clone(),
            None => {
                eprintln!("no values to sum");
                std::process::exit(1);
            }
        };

        for v in iter {
            value += v;
        }

        value
    }
}
